const express = require('express')
const router = express.Router()

const TaskController = require('../controllers/TaskController.js')

router.post('/create',(request,response) => {
	TaskController.createTask(request.body).then((result)=>{
		response.send(result)
	})
})

// Get all tasks
router.get('/', (request, response) => {
	TaskController.getTasks().then((result)=>{
		response.send(result)
	})
})

// Update a task
router.patch('/:id/update',(request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})


// Delete a task
/*
router.delete('/:id/delete',(request,response) => {
	TaskController.deleteTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

module.exports = router
*/

router.delete('/:id/delete',(request,response) => {
	TaskController.deleteTask(request.params.id).then((result) => {
		response.send(result)
	})
})

module.exports = router

router.get('/:id', (request, response) => {
	TaskController.getSpecificTask(request.params.id).then((result)=>{
		response.send(result)
	})
})

router.patch('/:id/complete',(request, response) => {
	TaskController.completedTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})