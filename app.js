// Importing the dependencies/modules
const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const taskRoute = require('./routes/taskRoutes.js')

dotenv.config()

// Server Setup
const app = express()
const port = 3003
app.use(express.json())
app.use(express.urlencoded({extended:true}))

// MongoDB Connection
mongoose.connect(`mongodb+srv://cinnamontoast:${process.env.MONGODB_PASSWORD}@cluster0.4pgcafz.mongodb.net/S36-todo?retryWrites=true&w=majority`,{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error',() => console.error(`Connection Error`))
db.on('open',() => console.log(`Connected to MongoDB!`))

// Handles route
app.use('/tasks', taskRoute)

// Server listening
app.listen(port, () => console.log(`Server is running at localHost: ${port}`))